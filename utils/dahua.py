import socket
import struct
import time


LOGIN_TEMPLATE = b'\xa0\x00\x00\x60%b\x00\x00\x00%b%b%b%b\x04\x01\x00\x00\x00\x00\xa1\xaa%b&&%b\x00Random:%b\r\n\r\n'
GET_SERIAL = b'\xa4\x00\x00\x00\x00\x00\x00\x00\x07\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' \
             b'\x00\x00\x00\x00\x00\x00\x00'
GET_CHANNELS = b'\xa8\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' \
               b'\x00\x00\x00\x00\x00\x00\x00\x00'
GET_SNAPSHOT = b'\x11\x00\x00\x00(\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' \
               b'\x00\x00\x00\n\x00\x00\x00%b\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' \
               b'\x00\x00%b\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
JPEG_GARBAGE1 = b'\x0a%b\x00\x00\x0a\x00\x00\x00'
JPEG_GARBAGE2 = b'\xbc\x00\x00\x00\x00\x80\x00\x00%b'
TIMEOUT = 10


class DahuaController:
    def __init__(self, ip, port, login, password):
        self.serial = ''
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.settimeout(TIMEOUT)
        self.socket.connect((ip, port))
        self.socket.send(LOGIN_TEMPLATE % (struct.pack('b', 24 + len(login) + len(password)), login.encode('ascii'),
                                           (8 - len(login)) * b'\x00', password.encode('ascii'),
                                           (8 - len(password)) * b'\x00', login.encode('ascii'),
                                           password.encode('ascii'), str(int(time.time())).encode('ascii')))
        data = self.socket.recv(128)
        if data[8] == 1:
            if data[9] == 4:
                self.status = 2
            self.status = 1
        elif data[8] == 0:
            self.status = 0
        else:
            self.status = -1
        if self.status == 0:
            self.socket.send(GET_SERIAL)
            self.serial = self.receive_msg().split(b'\x00')[0].decode('ascii')
            if self.serial == '':
                self.serial = '<unknown>'

    def get_channels_count(self):
        self.socket.send(GET_CHANNELS)
        channels = self.receive_msg()
        return channels.count(b'&&') + 1

    def receive_msg(self):
        header = self.socket.recv(32)
        length = struct.unpack('<H', header[4:6])[0]
        data = self.socket.recv(length)
        return data

    def get_snapshot(self, channel_id):
        channel_id = struct.pack('B', channel_id)
        self.socket.send(GET_SNAPSHOT % (channel_id, channel_id))
        self.socket.settimeout(3)
        data = self.receive_msg_2(channel_id)
        self.socket.settimeout(TIMEOUT)
        return data

    def receive_msg_2(self, c_id):
        garbage = JPEG_GARBAGE1 % c_id
        garbage2 = JPEG_GARBAGE2 % c_id
        data = b''
        i = 0
        while True:
            buf = self.socket.recv(1460)
            if i == 0:
                buf = buf[32:]
            data += buf
            if b'\xff\xd9' in data:
                break
            i += 1
        while garbage in data:
            t_start = data.find(garbage)
            t_end = t_start + len(garbage)
            t_start -= 24
            trash = data[t_start:t_end]
            data = data.replace(trash, b'')
        while garbage2 in data:
            t_start = data.find(garbage2)
            t_end = t_start + 32
            trash = data[t_start:t_end]
            data = data.replace(trash, b'')
        return data
